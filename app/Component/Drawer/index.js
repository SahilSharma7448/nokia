import React, { Component } from 'react';
import { useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, Alert, SafeAreaView } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';
import * as Utility from "../../utility/index"
// var WalletRoute=''
// var MyWalletRoute=''

// const Drawer =({navigation})=>{


//  useEffect(

//   () => {

//        return ()=>{
//         navigation.dispatch(
//             StackActions.reset({
//             index: 0,
//             actions: [NavigationActions.navigate({ routeName: "BottomTab" })]
//         })
//     ),
//         navigation.navigate("BottomTab")
//    } 

// }  )

// const Profile = () => {

//     navigation.dispatch(
//         StackActions.reset({
//             index: 0,
//             actions: [NavigationActions.navigate({ routeName: 'Profile' })]
//         })
//     );
//     navigation.navigate('Profile')
//     }
export default class Drawer extends Component {
    constructor(props) {
        super(props);

        this.state = {};

    }
    componentDidUpdate() {
        // const isDrawerOpen = this.props.navigation.state.isDrawerOpen;
        // if (!isDrawerOpen) {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Home' })]
            })
        );
        this.props.navigation.navigate('BottomTab');
    }
    // }

    MyWallet = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'MyWallet' })]
            })
        );
        this.props.navigation.navigate('MyWallet')
    }
    Privacy = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Privacy' })]
            })
        );
        this.props.navigation.navigate('Privacy')
    }

    MyAccount = () => {
        console.log("komal profile")
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'MyAccount' })]
            })
        );
        this.props.navigation.navigate('MyAccount')
    }
    Terms = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Terms' })]

            })
        );
        this.props.navigation.navigate('Terms')
    }
    Logout = () => {
        this.props.navigation.dispatch(
            StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Logout' })]
            })
        );
        this.props.navigation.navigate('Logout')
    }

    render() {
        return (
            <View>
                <View style={{ backgroundColor: "yellow", height: "40%", paddingLeft: "10%" }}>
                    <Image source={require("../../../assets/profile.png")} style={{ height: "48%", width: "35%" }} resizeMode="contain"></Image>
                    <Text style={{ fontSize:25,fontWeight:"bold" }}>james Morgan</Text>
                    <Text style={{ fontSize:15 }}>jamesmorgan@gmail.com</Text>
                </View>
                <View >
                    <View style={{ flexDirection: "row",marginTop:15,justifyContent:"flex-start"}}>
                        <Image source={require("../../../assets/user.png")} style={{ height:20, width:21,marginLeft:20,marginTop:6}} ></Image>
                        <TouchableOpacity onPress={() => this.MyAccount()}>
                            <Text style={{ fontSize: 20,marginLeft:20,marginTop:6}}>My Account</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 20, marginRight: 9, flexDirection: "row", justifyContent:"flex-start" }}>
                        <Image source={require("../../../assets/wallet.png")} style={{ height: 20, width:21,marginLeft:20,marginTop:6 }}></Image>
                        <TouchableOpacity onPress={() => this.MyWallet()}>
                            <Text style={{ fontSize: 20,marginLeft:20,marginTop:6 }}>My Wallet</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 22, marginLeft: 5, flexDirection: "row", justifyContent:"flex-start" }}>
                        <Image source={require("../../../assets/privacyicon.png")} style={{ height: 25, width: 21,marginLeft:15,marginTop:6 }}></Image>
                        <TouchableOpacity onPress={() => this.Privacy()}>
                            <Text style={{ fontSize: 20,marginLeft:12,marginTop:6 }}> Privacy Policy</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 22, marginLeft: 5, flexDirection: "row", justifyContent:"flex-start" }}>
                        <Image source={require("../../../assets/terms.png")} style={{ height: 25, width:21,marginLeft:20,marginTop:6 }}></Image>
                        <TouchableOpacity onPress={() => this.Terms()}>
                            <Text style={{ fontSize: 20,marginLeft:20,marginTop:6 }}>Terms and Conditions</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 22, marginLeft: 5, flexDirection: "row", justifyContent:"flex-start" }}>

                        <Image source={require("../../../assets/terms.png")} style={{ height: 25, width:21,marginLeft:20,marginTop:6 }}></Image>
                        <TouchableOpacity onPress={() => this.Logout()}>
                            <Text style={{ fontSize: 20,marginLeft:20,marginTop:6 }}>Logout</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>



        )
    }

}
