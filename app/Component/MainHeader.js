
import React from 'react';

import { Image, TouchableOpacity, Text, View } from "react-native"
import { DrawerActions } from 'react-navigation-drawer'
const MainHeader = ({ navigation,showBackIcon}) => {


    return (
        <View>


            <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 30 }}>
                {showBackIcon == true ?

                    <TouchableOpacity>
                        <Image source={require("../../assets/back.png")} style={{ height: 36, width: "100%", marginTop: 30, marginLeft: 35, }}></Image>
                    </TouchableOpacity>

                    :
                    <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
                        <Image source={require("../../assets/icon.png")} style={{ height: 36, width: 40, marginTop: 30, marginLeft: 35, }}></Image>
                    </TouchableOpacity>
                }
                <Text style={{
                    marginTop: 30, fontSize: 35, color: "orange", fontWeight: "bold"
                    , textAlign: "center"}}>Chess Game</Text>
                <TouchableOpacity>
                    <Image source={require("../../assets/logout.png")} style={{height: 36, width: "57%", marginTop: 30,
                     marginRight: 60,}}></Image>
                </TouchableOpacity>
            </View>

        </View>

    );

}
export default MainHeader;