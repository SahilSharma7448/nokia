
import React, { Component } from 'react';

import {
  Alert,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Linking,
  Platform,
  TouchableOpacity
} from "react-native"

import Modal from 'react-native-modal'
import MainHeader from "../../Component/MainHeader"
import { PermissionsAndroid } from 'react-native';
import CallLogs from 'react-native-call-log'
import { } from 'react-native-gesture-handler';


class ModalScreens extends Component {

  constructor(props) {

    super(props);
    this.state = {
      data: [
        { key: 'Android' }, { key: 'iOS' }, { key: 'Java' }, { key: 'Swift' },
        { key: 'Php' }, { key: 'Hadoop' }, { key: 'Sap' },
        { key: 'Python' }, { key: 'Ajax' }, { key: 'C++' },
        { key: 'Ruby' }, { key: 'Rails' }, { key: '.Net' },
        { key: 'Perl' }, { key: 'Sap' }, { key: 'Python' },
        { key: 'Ajax' }, { key: 'C++' }, { key: 'Ruby' },
        { key: 'Rails' }, { key: '.Net' }, { key: 'Perl' }],
      // name: ""

    }

  }




  // componentDidMount =  async() => {
  //   try {
  //     const granted = await PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
  //       // {
  //       //   title: 'Call Log Example',
  //       //   message:
  //       //     'Access your call logs',
  //       //   buttonNeutral: 'Ask Me Later',
  //       //   buttonNegative: 'Cancel',
  //       //   buttonPositive: 'OK',
  //       // }
  //     )
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       console.log(CallLogs);
  //       CallLogs.loadAll().then(c => console.log(c));
  //     } else {
  //       console.log('Call Log permission denied');
  //     }
  //   }

  //   catch (e) {
  //     console.log(e);
  //   }

  //  }


  // state = {
  //   modalVisible: false,

  // };

  // setModalVisible = (visible, item) => {
  //   this.setState({
  //     name: item.key,
  //     modalVisible: visible
  //   });
  // }


  //   dialCall = (number) => {
  //     let phoneNumber = '';
  //     if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
  //     else {phoneNumber = `telprompt:${number}`; }
  //     Linking.openURL(phoneNumber);
  //  };
  dialCall = (number) => {
    let SMS = '';
    if (Platform.OS === 'android') {SMS  = `sms:${number}`; }
    else {SMS = `smsprompt:${number}`; }
    Linking.openURL(SMS);
 };
  
    

render() {
  // const { modalVisible } = this.state;


  return (
    <View style={styles.Viewed}>
      {/* <MainHeader navigation={this.props.navigation}></MainHeader> */}

      <View>



        {/* <Modal
            // transparent={true}
            isVisible={modalVisible}
            //  backdropOpacity={0.5}
            hasBackdrop={true}
            // isVisible={this.state.isModalVisible}
            onBackdropPress={() => this.setModalVisible(!modalVisible, "")}
            onRequestClose={() => this.setModalVisible(!modalVisible, "")}

          > */}
        {/* 
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>{this.state.name}</Text>

                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: "red" }}
                  onPress={() => {
                    this.setModalVisible(!modalVisible, "");
                  }}
                >
                  <Text style={styles.textStyle}>Hide Modal</Text>
                </TouchableHighlight>
              </View>
            </View>
 */}
        {/* 
          </Modal> */}

        {/* <TouchableHighlight
            style={styles.openButton}
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <Text style={styles.textStyle}>Show Modal</Text>
          </TouchableHighlight> */}
        {this.state.data.map((item) => (
          <TouchableOpacity onPress={() => { this.dialCall(123456789) }}>

            <View style={{ marginLeft: 30 }}>
              <Text style={{ fontSize: 20 }}>{item.key}</Text>

            </View>
          </TouchableOpacity>
        ))}

      </View>
    </View>

  );
}
}
const styles = StyleSheet.create({


  Viewed: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,


  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,

  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default ModalScreens;