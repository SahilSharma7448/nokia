import React, { Component } from 'react';
import * as Utility from "../../utility"

import { Image  ,Text, View,ScrollView, TouchableOpacity} from "react-native"
export default class Home extends Component {
  constructor(props){
      super(props);
      this.state={
        name:"",
        email:"",
          usereList:[

            {name:"sahil",email:"sahil@gmail.com"},
            
            {name:"sharma",email:"sahil@gmail.com"},
            {name:"komal",email:"sahil@gmail.com"},
            {name:"dhhhhhd",email:"sahil@gmail.com"},
            {name:"khbhj",email:"sahil@gmail.com"},

          ]
      }
  }
  getUserData=(item)=>{
console.log("check item",item)
this.props.navigation.navigate("UserProfile",{
  name:item.name,
  email:item.email
})
  }

componentDidMount(){
  this.getData()
}

  getData=async()=>{
  let userName=  await Utility.getFromLocalStorge("name")
  let useremail=await Utility.getFromLocalStorge("email")
  this.setState({
    name:userName,
    email:useremail
  })
  }
  render() {
    
    return (
<View>
  <Text style={{textAlign:"center",fontSize:30,color:"blue"}}>{this.state.name}</Text>
  <Text style={{textAlign:"center",fontSize:30,color:"green"}}>{this.state.email}</Text>
    <ScrollView>
   {  this.state.usereList.map((item)=>(  
<TouchableOpacity onPress={()=>this.getUserData(item)}>
    <View style={{borderRadius:10,borderColor:"red",borderWidth:1,alignSelf:"center",width:"90%",marginTop:30,flexDirection:"row"}}>
        <View style={{width:"40%",}}>
        <Image source = {{uri:"https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png"}}
      style={{height:40,width:"30%",marginLeft:"5%",marginTop:20,bottom:10}}></Image>
        </View>
        <View style={{width:"50%"}}>
       <Text
    
    style={{fontSize:20,color:"black",marginLeft:"40%"}}>Name:-{item.name}</Text>  
      
    <Text style={{fontSize:20,color:"black",marginLeft:"40%",width:"80%"}}>email:-{item.email}</Text> 
    
 
    </View>
    </View>
    </TouchableOpacity>
    ))}
  </ScrollView>
    </View>
    );
  }
}
