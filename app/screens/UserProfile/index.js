
import React, { Component } from 'react';

import { Image    ,TouchableOpacity  , Text, View} from "react-native"
import * as Utility from "../../utility"
export default class UserProfile extends Component {
  
    constructor(props){
        super(props);
        this.state={
           name:this.props.navigation.state.params.name,
           email:this.props.navigation.state.params.email,
           value:"left"
        }
    }
    changeColor=(data)=>{
this.setState({
  value:data
})
    }

    submit=async()=>{
      await Utility.setInLocalStorge("name","komal")
      await Utility.setInLocalStorge("email","komal@gmail.com")
    }
  render() {
    return (
<View>

<View style={{flexDirection:"row",width:240,height:40,alignSelf:"center",alignItems:"center",justifyContent:"center",borderRadius:20}}>
<TouchableOpacity onPress={()=>this.changeColor("left")}> 

<View style={{height:40,width:120,marginTop:30,backgroundColor:this.state.value=="left"?"skyblue":"white",borderTopLeftRadius:20,borderBottomLeftRadius:20}}>

<Text style={{fontSize:20,alignSelf:"center",marginTop:5}}>Left</Text>

</View>

</TouchableOpacity>
<TouchableOpacity onPress={()=>this.changeColor("right")}> 

<View style={{height:40,width:120,marginTop:30,backgroundColor:this.state.value=="right"?"red":"white",borderTopRightRadius:20,borderBottomRightRadius:20
}}>
  <Text style={{fontSize:20,alignSelf:"center",marginTop:5}}>Right</Text>



</View>
</TouchableOpacity>


</View>




    <View  style={{width:"80%",marginLeft:"10%",marginRight:"15%",marginTop:40,borderColor:"red",borderWidth:2}}>
        <Image source={{uri:"https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png   "}} 
        style={{width:"36%",height:102,marginLeft:"2%",marginRight:"2%",marginTop:20}}>

        </Image>
<Text style={{fontSize:20,marginLeft:"45%",bottom:120}}>Name:-{this.state.name}</Text>
<Text style={{fontSize:20,marginLeft:"45%",bottom:100}}>email:-{this.state.email}</Text>


    </View>
    <TouchableOpacity onPress={()=>this.submit()}> 
    <View style={{height:40,width:"60%",backgroundColor:"yellow",marginTop:30,alignSelf:"center"}}> 
<Text style={{fontSize:20,alignSelf:"center",marginTop:5}}>submit</Text>

</View>
</TouchableOpacity>


    
  
    
    </View>
    );
  }
}