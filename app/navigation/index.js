
import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from "../screens/Splash"
import SignUp from "../screens/SignUp"
import Register from "../screens/Register"
import Home from "../screens/Home"
import UserProfile from "../screens/UserProfile"
import ReportScreen from "../screens/Report/index"
import ProductScreen from "../screens/Product/index"
import Drawer from "../Component/Drawer"
import MyAccount from "../screens/MyAccount"
import Privacy from "../screens/Privacy"
import Logout from "../screens/Logout"
import Terms from "../screens/Terms"
import MyWallet from "../screens/MyWallet"
import ModalScreens from "../screens/ModalScreens"
import AnimationScreen from "../screens/AnimationScreen"
// import Loader from  "../screens/Loader"
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Alert,
  TouchableOpacity,
  Image,
  ImageBackground,


} from 'react-native'
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer'
const AppStack = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        headerShown: false,
      },
    },
    Register: {
      screen: Register,
      navigationOptions: {
        headerShown: false,
      }
    },
    Home: {
      screen: Home,
      navigationOptions: {
        headerShown: false,
      }
    },
    UserProfile: {
      screen: UserProfile,
      navigationOptions: {
        headerShown: false,
      }
    },

    Report: {
      screen: ReportScreen,
      navigationOptions: {
        headerShown: false,
      }
    },

    MyAccount: {
      screen:MyAccount,
      navigationOptions: {
        headerShown: false,

      }
    },
    Logout: {
      screen: Logout,
      navigationOptions: {
        headerShown: false,
      }
    },
    Privacy: {
      screen:Privacy,
       navigationOptions: {
        headerShown: false,
      }
    },

    Terms: {
      screen:Terms,
       navigationOptions: {
        headerShown: false,
      }
    },

    MyWallet: {
      screen:MyWallet,
       navigationOptions: {
        headerShown: false,
      }
    },
    ModalScreens: {
      screen:ModalScreens,
       navigationOptions: {
        headerShown: false,
      }
    },
    AnimationScreen: {
      screen:AnimationScreen,
       navigationOptions: {
        headerShown: false,
      }
    },
    // Loader : {
    //   screen:Loader,
    //    navigationOptions: {
    //     headerShown: false,
    //   }
    // },

  },
  {

    initialRouteName: 'Register',
  }
);

const DrawerNavigator = createDrawerNavigator({
  Report: {
    screen: ReportScreen,
  },

},
  {
    contentOptions: {
      style: {
        backgroundColor: "black",
        flex: 1,
      }
    },
    navigationOptions: {
      drawerLockMode: 'locked-closed'
    },
    contentComponent: Drawer,
  },
);

////////////////////////////////////////////// bottom navigation/////////////////////////////////
////////////////////////////////////////////// bottom navigation/////////////////////////////////
const TabNavigator = createBottomTabNavigator({
  tab1: {
    screen: ReportScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "UserProfile ",
      tabBarIcon: ({ focused, tintColor }) => {

        return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}>


          <View>
            <Text >{"report"}</Text></View>
        </View>
      }
    })
  },
  //////////////////////////define initial route//////////////////////////////////////////



  tab2: {
    screen: ProductScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "Home ",
      tabBarIcon: ({ focused, tintColor }) => {

        return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}>

          <View>
            <Text style={{ textAlign: "center" }} >product</Text></View>
        </View>
      }
    })
  },
},
  {
    initialRouteName: "tab1",
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: 'white',
      activeBackgroundColor: 'white',
      inactiveTintColor: 'white',
      inactiveBackgroundColor: 'white',
      showIcon: true,
      showLabel: true,
      style: { height: 50, borderTopColor: 'white', borderTopWidth: 1, elevation: 20 },
      labelStyle: {
        fontSize: 1,
        fontWeight: "bold",

      }

    }
  }
);
const Routes = createAppContainer(
  createSwitchNavigator({
    App: AppStack,
    Drawer: DrawerNavigator,
    BottomTab: TabNavigator,
  }),
);
export default Routes;